require('dotenv').config()
const path = require('path')
const express = require('express')
const spreadsheet = require('./spreadsheet')

const port = process.env.PORT || 4500 // heroku sets port dynamically, do not set it in your variables
const app = express()

app.get('/api/get-data', (req, res) => {
  return res.send({ data: 'API works!' })
})

app.get('/api/get-spreadsheet', async (req, res) => {
  try {
    const response = await spreadsheet.getSheet()
    return res.status(200).send({ data: response })
  } catch (error) {
    return res.status(500).send({ error })
  }
})

app.listen(port, () => {
  console.info(`Express listening at http://localhost:${port}`)
  console.info(`serving bundled content from ${path.join(__dirname, '../public')}`)
})
