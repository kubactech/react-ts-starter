const { GoogleSpreadsheet } = require('google-spreadsheet')

const decodeKey = (key) => {
  // decode from base64
  const buff = Buffer.from(key, 'base64')
  const decodedKey = buff.toString('utf8')

  // treat newlines correctly
  return decodedKey.replace(/\\n/g, '\n')
}

exports.getSheet = async () => {
  const doc = new GoogleSpreadsheet(process.env.SHEET_ID)

  await doc.useServiceAccountAuth({
    client_email: process.env.SPREADSHEET_EMAIL,
    private_key: decodeKey(process.env.SPREADSHEET_KEY_ENCODED)
  })

  await doc.loadInfo()
  return doc.title
}
