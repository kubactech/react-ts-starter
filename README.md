# React TS starter kit

### Getting started

1. clone repository
2. `npm install`
3. `npm run dev`

### Features 
- webpack dev server and basic production config
- typescript
- eslint
- CSS modules and stylelint
- basic Express server
- Gitlab CI integration
- Googlesheets API integration
- Heroku integration

### Heroku integration: 
- you can deploy with pre-configured Gitlab pipeline
- make sure you have following variables set in your Gitlab project

```
HEROKU_API_KEY = your api key for Heroku account
HEROKU_APP_DEV = name of your application where you whant your "dev" branch to be deployed
HEROKU_APP_PROD = name of your application where you whant your "master" branch to be deployed
```

### Googlesheets API:
- you can use the API as basic "database"
- you need to have following in your envs:

```
SPREADSHEET_KEY_ENCODED = your api key encoded by base64 (it is encoded to prevent usual problems with copy pasting long keys as string)
SPREADSHEET_EMAIL = your service account
SHEET_ID = id of the sheet
```

enjoy